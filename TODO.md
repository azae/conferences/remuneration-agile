

Nos objectifs:
- c'est possible
- pourquoi le faire

Exemple

Contre-exemple
- gravity payement
- smartesting

Pourquoi ?
- (auto organisation -> people over process)
- marqueur d'un véritable engagement des propriétaires et des salariés sur la people over process
  - (acces à un truc tabou pour les salariés)
  - lacher du contrôle pour les propriétaires
  - sortir du statut de victime, devenir co responsable de l'avenir de l'entreprise et de sa satisfaction personelle.
